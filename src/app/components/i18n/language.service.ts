import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { en_US, NzI18nService, zh_CN } from 'ng-zorro-antd/i18n';

@Injectable({
    providedIn: 'root'
})
export class LanguageService {
    constructor(
        private translate: TranslateService,
        private i18n: NzI18nService
    ) {
    }

    setLanguage(lang: string) {
        this.translate.use(lang);
        this.i18n.setLocale(LanguageService.getNzLocale(lang));
    }

    private static getNzLocale(lang: string) {
        switch (lang) {
            case 'en':
                return en_US;
            case 'zh':
                return zh_CN;
            default:
                return zh_CN;
        }
    }
}
