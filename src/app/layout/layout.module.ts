import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { RouteReuseStrategy, RouterModule } from '@angular/router';
import { NzBackTopModule } from 'ng-zorro-antd/back-top';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { TabRouteReuseService } from './tab-route-reuse.service';

@NgModule({
    declarations: [
        LayoutComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        NzLayoutModule,
        NzTabsModule,
        NzIconModule,
        NzMenuModule,
        NzBackTopModule,
        NzAvatarModule,
        NzBadgeModule,
        NzToolTipModule,
        NzDropDownModule
    ],
    providers: [
        {
            provide: RouteReuseStrategy,
            useExisting: TabRouteReuseService
        }
    ]
})
export class LayoutModule {
}
