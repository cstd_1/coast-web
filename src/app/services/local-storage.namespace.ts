// 全局命名空间
export const GLOBAL_NAMESPACE = 'tdm.';

// 默认设置
export const LANG = GLOBAL_NAMESPACE + 'lang'
// 用户信息
export const USER_NAMESPACE = GLOBAL_NAMESPACE + 'user.';
export const USER_INFO = USER_NAMESPACE + 'id';
export const MENU_LIST = USER_NAMESPACE + 'menuList';
export const PERMISSION = USER_NAMESPACE + 'permission';

