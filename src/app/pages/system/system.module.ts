import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SystemRoutingModule } from './system-routing.module';
import { ModelDesignerComponent } from './model-designer/model-designer.component';
import { DataEditorComponent } from './data-editor/data-editor.component';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { FormsModule } from '@angular/forms';


@NgModule({
    declarations: [
        ModelDesignerComponent,
        DataEditorComponent
    ],
    imports: [
        CommonModule,
        SystemRoutingModule,
        NzCheckboxModule,
        FormsModule
    ]
})
export class SystemModule {
}
