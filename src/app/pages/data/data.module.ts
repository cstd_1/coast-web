import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataRoutingModule } from './data-routing.module';
import { DetailComponent } from './detail/detail.component';
import { GridComponent } from './grid/grid.component';
import {NzInputModule} from 'ng-zorro-antd/input';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DetailComponent,
    GridComponent
  ],
    imports: [
        CommonModule,
        DataRoutingModule,
        NzInputModule,
        FormsModule
    ]
})
export class DataModule { }
