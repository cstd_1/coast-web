import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { TaskComponent } from './task/task.component';
import { NoticeComponent } from './notice/notice.component';
import { TranslateModule } from '@ngx-translate/core';
import {NzPaginationModule} from 'ng-zorro-antd/pagination';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { FormsModule } from '@angular/forms';
import { NzSwitchModule } from 'ng-zorro-antd/switch';


@NgModule({
    declarations: [
        TaskComponent,
        NoticeComponent
    ],
    imports: [
        CommonModule,
        HomeRoutingModule,
        TranslateModule,
        NzPaginationModule,
        NzDatePickerModule,
        FormsModule,
        NzSwitchModule
    ]
})
export class HomeModule {
}
